<?php

function ludocaz_supports()
{
  add_theme_support('title-tag');
  add_theme_support('post-thumbnails');
  add_theme_support('menus');
}

function ludocaz_register_assets()
{
  wp_register_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css',);
  wp_register_script('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js');
  wp_enqueue_style('bootstrap');
}

add_action('after_setup_theme',  'ludocaz_supports');
add_action('wp_enqueue_scripts', 'ludocaz_register_assets');

function my_customizer_settings($wp_customize)
{
  $wp_customize->add_setting('logo_upload');
}
add_action('customize_register', 'my_customizer_settings');

function my_customizer_controls($wp_customize)
{
  $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_upload', array(
    'label'    => 'Logo du site',
    'section'  => 'title_tagline',
    'settings' => 'logo_upload',
  )));
}
add_action('customize_register', 'my_customizer_controls');

function montheme_theme_update($transient)
{
  if (empty($transient->checked)) {
    return $transient;
  }
  $theme_data = wp_get_theme(wp_get_theme()->template);
  $version = $theme_data->get('Version');
  $remote_version = $version;
  if (false !== strpos($remote_version, '-')) {
    list($remote_version) = explode('-', $remote_version);
  }
  $theme_slug = $theme_data->get_stylesheet();
  if ($theme_slug === 'theme-principal') {
    $repo_url = 'https://gitlab.com/ludocaz1/theme-principal';
    $zip_url = $repo_url . '/repository/archive.zip?ref=master';
    $response = wp_remote_get($zip_url);
    if (!is_wp_error($response) && $response['response']['code'] === 200) {
      $zip_file = wp_upload_dir()['basedir'] . '/' . $theme_slug . '-update.zip';
      file_put_contents($zip_file, $response['body']);
      $transient->response[$theme_slug] = array(
        'theme' => $theme_slug,
        'new_version' => $remote_version,
        'url' => $theme_data->get('ThemeURI'),
        'package' => $zip_file
      );
    }
  }
  return $transient;
}
add_filter('pre_set_site_transient_update_themes', 'montheme_theme_update');

function montheme_update_theme($upgrader_object, $options)
{
  if ($options['action'] == 'update' && $options['type'] === 'theme') {
    $theme_slug = $options['themes'][0];
    if ($theme_slug === 'theme-principal') {
      $theme_dir = get_theme_root() . '/' . $theme_slug;
      WP_Filesystem();
      $result = unzip_file($options['package'], $theme_dir);
      if (!is_wp_error($result)) {
        $theme_data = wp_get_theme($theme_slug);
        $theme_data->set('Version', $options['new_version']);
        $theme_data->save();
      }
    }
  }
}
add_action('upgrader_process_complete', 'montheme_update_theme', 10, 2);
